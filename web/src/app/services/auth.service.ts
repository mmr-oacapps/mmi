import { Injectable } from '@angular/core';
import {User} from '../model/user';
import {JwtResponse} from '../model/jwt-response';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Observable, BehaviorSubject, pipe} from 'rxjs';
import {Unit} from '../model/unit';
import {UnitResponse} from '../model/unitResponse';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  AUTH_SERVER = 'http://localhost:8080/api/v.0.0.1';
  authSubject = new BehaviorSubject(false);

  constructor(private httpClient: HttpClient) { }

  public login(userInfo: User): Observable<JwtResponse> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Basic ' + btoa(userInfo.email + ':' + userInfo.password)
      })
    };
    return this.httpClient.post(this.AUTH_SERVER + '/authentication/login',"",httpOptions).pipe(
      tap(async (res: JwtResponse) => {
        if (res.user) {
          localStorage.setItem('ACCESS_TOKEN', res.user.access_token);
          localStorage.setItem('EXPIRES_IN', res.user.expires_in);
          this.authSubject.next(true);
        }
      })
    );
  }

  public units(unit: Unit): Observable<UnitResponse> {

    return this.httpClient.post(this.AUTH_SERVER + '/converter/convert', unit).pipe(
      tap(async (res: UnitResponse) => {})
    );
  }

  isAuthenticated() {
    return  this.authSubject.asObservable();
  }

  public logout() {
    localStorage.removeItem('ACCESS_TOKEN');
    localStorage.removeItem('EXPIRES_IN');
    this.authSubject.next(false);
  }
}
