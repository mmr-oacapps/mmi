export interface UnitResponse {
  from: string;
  to: string;
  value: string;
  result: string;
}
