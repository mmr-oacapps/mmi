export interface JwtResponse {
  user: {
    email: string,
    access_token: string,
    expires_in: string
  }
}

