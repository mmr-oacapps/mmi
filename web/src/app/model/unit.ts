export interface Unit {
  convertUnit: string;
  from: string;
  to: string;
  value: string;
}
