import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.css']
})
export class UnitsComponent implements OnInit {

  lengthForm: FormGroup;
  masssForm: FormGroup;
  temperaturesForm: FormGroup;

  lengthResult: any;
  massResult: any;
  temperatureResult: any;

  lengths = [
    { code: 'in', name: 'Inches'},
    { code: 'ft', name: 'Feet'},
    { code: 'mi', name: 'Miles'},
    { code: 'mm', name: 'Millimeters'},
    { code: 'cm', name: 'Centimeters'},
    { code: 'm',  name: 'Meters'},
    { code: 'km', name: 'Kilometers'}
  ];

  masss = [
    { code: 'tn', name: 'Tonne'},
    { code: 'kg', name: 'Kilogram'},
    { code: 'gm', name: 'Gram'},
    { code: 'ou', name: 'Ounce'},
    { code: 'po', name: 'Pound'},
    { code: 'st', name: 'Stone'}
  ];

  temperatures = [
    { code: 'f', name: 'Fahrenheit'},
    { code: 'c', name: 'Celsius'},
    { code: 'k', name: 'Kelvin'}
  ];



  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.lengthForm = this.formBuilder.group({
      value: [0.0, Validators.required],
      from: ['m', Validators.required],
      to: ['km', Validators.required],
      convertUnit: ['length'],
      result: [0.0]
    });

    this.masssForm = this.formBuilder.group({
      value: [0.0, Validators.required],
      from: ['gm', Validators.required],
      to: ['kg', Validators.required],
      convertUnit: ['mass'],
      result: [0.0]
    });

    this.temperaturesForm = this.formBuilder.group({
      value: [0.0, Validators.required],
      from: ['c', Validators.required],
      to: ['f', Validators.required],
      convertUnit: ['temperature'],
      result: [0.0]
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

  lengthSubmit() {
    this.authService.units(this.lengthForm.value).subscribe((res) => {
     this.lengthResult = res.result + ' ' + res.to;
    });
  }
  massSubmit() {
    this.authService.units(this.masssForm.value).subscribe((res) => {
      this.massResult = res.result + ' ' + res.to;
    });
  }
  temperatureSubmit() {
    this.authService.units(this.temperaturesForm.value).subscribe((res) => {
      this.temperatureResult = res.result + ' ' + res.to;
    });
  }

}
