package com.mmr.mmi.api.unit_converter;

import com.mmr.mmi.api.pojo.Unit;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LengthTest {

    Length length = new Length();

    @Test
    public void getConversion() {

        Unit unit = new Unit();
        unit.setConvertUnit("length");
        unit.setFrom("m");
        unit.setTo("km");
        unit.setValue(12000);

        double result = length.getConversion(unit);

        Assert.assertEquals(12, result, 0);
        Assert.assertNotEquals(1, result);

    }

    @Test
    public void toUnit() {

        double result = length.toUnit("km", 0.5);

        Assert.assertEquals(500, result, 0);
        Assert.assertNotEquals(1, result);
    }

    @Test
    public void fromUnit() {

        double result = length.fromUnit("km", 500);

        Assert.assertEquals(0.5, result, 0);
        Assert.assertNotEquals(1, result);
    }
}
