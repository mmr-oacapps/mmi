package com.mmr.mmi.api.Enum;

import lombok.Getter;

@Getter
public enum MassEnum {

    TONNE("tn", "TONNE"),
    KILOGRAM("kg", "KILOGRAM"),
    GRAM("gm", "GRAM"),
    OUNCE("ou", "OUNCE"),
    POUND("po", "POUND"),
    STONE("st", "STONE");

    private final String code;
    private final String name;


    MassEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

}
