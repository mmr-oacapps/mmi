package com.mmr.mmi.api.unit_converter;

import com.mmr.mmi.api.interfaces.Convert;
import com.mmr.mmi.api.pojo.Unit;

public class Length implements Convert {

    static double INCHES = 0.0254001;
    static double FEET = 0.3048;
    static double MILES = 1609.35;
    static double MILLIMETERS = 0.001;
    static double CENTIMETERS = 0.01;
    static double METERS = 1;
    static double KILOMETERS = 1000;

    @Override
    public double getConversion(Unit unit) {

        double meters = toUnit(unit.getFrom(), unit.getValue());
        double result = fromUnit(unit.getTo(), meters);
        return result;
    }

    @Override
    public double toUnit(String from, double val) {
        double meters;

        switch (from){
            case "in": meters = ( val * INCHES ); break;
            case "ft": meters = ( val * FEET ); break;
            case "mi": meters = ( val * MILES ); break;
            case "mm": meters = ( val * MILLIMETERS ); break;
            case "cm": meters = ( val * CENTIMETERS ); break;
            case "m":  meters = ( val * METERS ); break;
            case "km": meters = ( val * KILOMETERS ); break;
            default: meters = val; break;
        }
        return meters;
    }

    @Override
    public double fromUnit(String to, double val) {
        double result;

        switch (to){
            case "in": result = ( val / INCHES ); break;
            case "ft": result = ( val / FEET ); break;
            case "mi": result = ( val / MILES ); break;
            case "mm": result = ( val / MILLIMETERS ); break;
            case "cm": result = ( val / CENTIMETERS ); break;
            case "m":  result = ( val / METERS ); break;
            case "km": result = ( val / KILOMETERS ); break;
            default: result = val; break;
        }
        return result;
    }

}
