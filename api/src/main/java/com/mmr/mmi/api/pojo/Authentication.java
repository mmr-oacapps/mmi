package com.mmr.mmi.api.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Authentication {

    private boolean isSuccess;
    private UserResponse user;

}
