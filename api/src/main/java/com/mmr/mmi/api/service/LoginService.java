package com.mmr.mmi.api.service;

import com.mmr.mmi.api.Repository.LoginRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    LoginRepository loginRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginService.class);

    public boolean login(String userName, String password) {

        boolean loginSuccess = false;

        try {
            loginSuccess = loginRepository.existsByEmailAndPassword(userName, password);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

        return loginSuccess;

    }

}
