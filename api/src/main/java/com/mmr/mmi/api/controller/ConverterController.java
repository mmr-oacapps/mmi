package com.mmr.mmi.api.controller;

import com.mmr.mmi.api.factory.ConverterFactory;
import com.mmr.mmi.api.pojo.ConversionResult;
import com.mmr.mmi.api.pojo.Unit;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/converter")
public class ConverterController {

    @PostMapping("/convert")
    public ConversionResult convert(@RequestBody Unit unit){

        ConverterFactory  converterFactory = new ConverterFactory();
        double result = converterFactory.getConversion(unit);

        ConversionResult conversionResult = new ConversionResult();

        conversionResult.setFrom(unit.getFrom());
        conversionResult.setTo(unit.getTo());
        conversionResult.setGivenValue(unit.getValue());
        conversionResult.setResult(result);

        return conversionResult;
    }
}
