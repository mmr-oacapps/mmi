package com.mmr.mmi.api.controller;


import com.mmr.mmi.api.Enum.LoginEnum;
import com.mmr.mmi.api.pojo.Authentication;
import com.mmr.mmi.api.pojo.UserResponse;
import com.mmr.mmi.api.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

@RestController
@RequestMapping("/authentication")
public class UserAccessController {

    @Autowired
    LoginService loginService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConverterController.class);


    @PostMapping("/login")
    public Authentication login(@RequestHeader("Authorization") String authorization){
        Authentication authentication = new Authentication();

        boolean loginSuccess = false;

        try {
            if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
                String base64Credentials = authorization.substring("Basic".length()).trim();

                int userName = 0;
                int password = 1;

                byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
                String credentials = new String(credDecoded, StandardCharsets.UTF_8);

                final String[] credential = credentials.split(":", 2);

                if(credential[userName].isEmpty()){
                    LOGGER.error(LoginEnum.USER_NAME_EMAIL_MISSING.getMsg());
                }
                if(credential[password].isEmpty()){
                    LOGGER.error(LoginEnum.PASSWORD_MISSING.getMsg());
                }

                loginSuccess = loginService.login(credential[userName], credential[password]);

                authentication.setSuccess(loginSuccess);

                UserResponse user = new UserResponse();
                user.setEmail(credential[userName]);

                if(loginSuccess) {
                    user.setAccess_token(UUID.randomUUID().toString());
                    user.setExpires_in(100000);
                }

                authentication.setUser(user);

            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

        return authentication;
    }

}
