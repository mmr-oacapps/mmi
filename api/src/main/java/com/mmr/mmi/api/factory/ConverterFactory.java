package com.mmr.mmi.api.factory;

import com.mmr.mmi.api.pojo.Unit;
import com.mmr.mmi.api.interfaces.Convert;
import com.mmr.mmi.api.unit_converter.Length;
import com.mmr.mmi.api.unit_converter.Mass;
import com.mmr.mmi.api.unit_converter.Temperature;

public class ConverterFactory {
    
    public double getConversion(Unit unit) {

        Convert convert;
        
        double result = 0;
        
        switch (unit.getConvertUnit()){
            case "length" :
                convert = new Length();
                result = convert.getConversion(unit);
                break;
            case "mass" :
                convert = new Mass();
                result = convert.getConversion(unit);
                break;
            case "temperature" :
                convert = new Temperature();
                result = convert.getConversion(unit);
                break;
            default:
                break;
        }

        return result;
        
    }
}
