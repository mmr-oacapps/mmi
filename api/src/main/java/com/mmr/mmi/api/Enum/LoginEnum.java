package com.mmr.mmi.api.Enum;

import lombok.Getter;

@Getter
public enum LoginEnum {

    INVALID_EXPIRE_TOKEN(11, "Invalid or Token expire"),
    EMAIL_DOESNOT_EXISTS(12, "Email doesnot exists"),
    USER_NAME_EMAIL_MISSING(13, "Username missing from Authorization Header"),
    PASSWORD_MISSING(14, "Password missing from Authorization Header");

    private final int code;
    private final String msg;

    LoginEnum(int code, String msg){
        this.code = code;
        this.msg  = msg;
    }

}
