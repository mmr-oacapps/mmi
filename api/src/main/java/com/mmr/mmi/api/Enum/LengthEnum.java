package com.mmr.mmi.api.Enum;

import lombok.Getter;

@Getter
public enum LengthEnum {

    INCHES("in", "INCHES"),
    FEET("ft", "FEET"),
    MILES("mi", "MILES"),
    MILLIMETERS("mm", "MILLIMETERS"),
    CENTIMETERS("cm", "CENTIMETERS"),
    METERS("m", "METERS"),
    KILOMETERS("km", "KILOMETERS");


    private final String code;
    private final String name;


    LengthEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
