package com.mmr.mmi.api.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponse {

    private String email;
    private int expires_in;
    private String access_token;
}
