package com.mmr.mmi.api.pojo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Unit {

    private String convertUnit;
    private String from;
    private String to;
    private double value;
}
