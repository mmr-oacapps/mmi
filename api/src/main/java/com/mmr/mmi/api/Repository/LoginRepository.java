package com.mmr.mmi.api.Repository;

import com.mmr.mmi.api.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends JpaRepository<User, Long> {

    boolean existsByEmailAndPassword(String email, String password);


}
