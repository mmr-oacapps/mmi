package com.mmr.mmi.api.unit_converter;

import com.mmr.mmi.api.interfaces.Convert;
import com.mmr.mmi.api.pojo.Unit;

public class Temperature implements Convert {

    @Override
    public double getConversion(Unit unit) {

        double fahrenheit = toUnit(unit.getFrom(), unit.getValue());
        double result = fromUnit(unit.getTo(), fahrenheit);
        return result;
    }

    @Override
    public double toUnit(String from, double val) {
        double fahrenheit;

        switch (from){
            case "c": fahrenheit = ( ( val * 9/5 ) + 32 ); break;
            case "f": fahrenheit = ( val * 1 ); break;
            case "k": fahrenheit = ( ( ( val - 273.15 ) * 9/5 )+ 32 ); break;
            default:  fahrenheit = val; break;
        }
        return fahrenheit;
    }

    @Override
    public double fromUnit(String to, double val) {
        double result;

        switch (to){
            case "c": result = ( ( val - 32 ) * 5/9 ); break;
            case "f": result = ( val / 1 ); break;
            case "k": result = ( ( ( val - 32 ) * 5/9 ) + 273.15 ); break;
            default: result = val; break;
        }
        return result;
    }

}
