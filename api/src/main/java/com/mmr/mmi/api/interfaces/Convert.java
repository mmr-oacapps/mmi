package com.mmr.mmi.api.interfaces;

import com.mmr.mmi.api.pojo.Unit;

public interface Convert {

    double getConversion(Unit unit);

    double toUnit(String from, double val);

    double fromUnit(String to, double val);
}
