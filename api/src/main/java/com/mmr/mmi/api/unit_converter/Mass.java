package com.mmr.mmi.api.unit_converter;

import com.mmr.mmi.api.interfaces.Convert;
import com.mmr.mmi.api.pojo.Unit;

public class Mass implements Convert {

    static double TONNE = 1000;
    static double KILOGRAM = 1;
    static double GRAM = 0.001;
    static double OUNCE = 0.0283495;
    static double POUND = 0.453592;
    static double STONE = 6.35029;

    @Override
    public double getConversion(Unit unit) {

        double kilogram = toUnit(unit.getFrom(), unit.getValue());
        double result = fromUnit(unit.getTo(), kilogram);
        return result;
    }

    @Override
    public double toUnit(String from, double val) {
        double kilogram;

        switch (from){
            case "tn": kilogram = ( val * TONNE ); break;
            case "gm": kilogram = ( val * GRAM ); break;
            case "ou": kilogram = ( val * OUNCE ); break;
            case "po": kilogram = ( val * POUND ); break;
            case "st": kilogram = ( val * STONE ); break;
            default:   kilogram = ( val * KILOGRAM); break;
        }
        return kilogram;
    }

    @Override
    public double fromUnit(String to, double val) {
        double result;

        switch (to){
            case "tn": result = ( val / TONNE ); break;
            case "gm": result = ( val / GRAM ); break;
            case "ou": result = ( val / OUNCE ); break;
            case "po": result = ( val / POUND ); break;
            case "st": result = ( val / STONE ); break;
            default:   result = ( val / KILOGRAM); break;
        }
        return result;
    }
}
